From d636ddf960c7d006ec760f9452422310f55b4266 Mon Sep 17 00:00:00 2001
From: Zhaoyang Liu <zhaoyang@codeaurora.org>
Date: Wed, 14 Mar 2018 19:01:20 +0800
Subject: [PATCH 06/11] Enable CNSS PCI platform module

Enable CNSS PCI wlan device platform module on kernel 4.9

Change-Id: I92ed662e74ec21f98bd854d41b94f2a878619135
Signed-off-by: Zhaoyang Liu <zhaoyang@codeaurora.org>
---
 arch/arm/mach-imx/mach-imx6q.c       |  11 +
 drivers/net/wireless/cnss/cnss_pci.c | 387 ++++++++++++-----------------------
 2 files changed, 147 insertions(+), 251 deletions(-)

diff --git a/arch/arm/mach-imx/mach-imx6q.c b/arch/arm/mach-imx/mach-imx6q.c
index 0d0ae91..c2212cf 100644
--- a/arch/arm/mach-imx/mach-imx6q.c
+++ b/arch/arm/mach-imx/mach-imx6q.c
@@ -338,6 +338,16 @@ static void __init imx6q_init_machine(void)
 	imx6q_axi_init();
 
 	{
+		struct platform_device_info cnss_pci_devinfo = {
+			.name = "cnss",
+			.id = 0,
+			.res = NULL,
+			.num_res = 0,
+			.data = NULL,
+			.size_data = 0,
+			.dma_mask = 0,
+		};
+
 		struct platform_device_info cnss_sdio_devinfo = {
 			.name = "cnss_sdio",
 			.id = 0,
@@ -348,6 +358,7 @@ static void __init imx6q_init_machine(void)
 			.dma_mask = 0,
 		};
 
+		platform_device_register_full(&cnss_pci_devinfo);
 		platform_device_register_full(&cnss_sdio_devinfo);
 	}
 }
diff --git a/drivers/net/wireless/cnss/cnss_pci.c b/drivers/net/wireless/cnss/cnss_pci.c
index 2063145..26aaae6 100644
--- a/drivers/net/wireless/cnss/cnss_pci.c
+++ b/drivers/net/wireless/cnss/cnss_pci.c
@@ -9,8 +9,6 @@
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  */
-#include <asm/dma-iommu.h>
-#include <linux/iommu.h>
 #include <linux/export.h>
 #include <linux/err.h>
 #include <linux/of.h>
@@ -28,11 +26,15 @@
 #include <linux/sched.h>
 #include <linux/pm_qos.h>
 #include <linux/pm_runtime.h>
+#ifdef CONFIG_ESOC_CLIENT
 #include <linux/esoc_client.h>
+#endif
 #include <linux/firmware.h>
 #include <linux/dma-mapping.h>
+#ifdef CONFIG_MSM_BUS_SCALING
 #include <linux/msm-bus.h>
 #include <linux/msm-bus-board.h>
+#endif
 #include <linux/spinlock.h>
 #include <linux/suspend.h>
 #include <linux/rwsem.h>
@@ -41,11 +43,15 @@
 #include <linux/log2.h>
 #include <linux/etherdevice.h>
 #include <linux/msm_pcie.h>
+#ifdef CONFIG_MSM_SUBSYSTEM_RESTART
 #include <soc/qcom/subsystem_restart.h>
 #include <soc/qcom/subsystem_notif.h>
+#endif
+#ifdef CONFIG_MSM_MEMORY_DUMP
 #include <soc/qcom/ramdump.h>
-#include <net/cfg80211.h>
 #include <soc/qcom/memory_dump.h>
+#endif
+#include <net/cfg80211.h>
 #include <net/cnss.h>
 #include "cnss_common.h"
 
@@ -53,8 +59,6 @@
 #include <net/cnss_prealloc.h>
 #endif
 
-#define subsys_to_drv(d) container_of(d, struct cnss_data, subsys_desc)
-
 #define VREG_ON			1
 #define VREG_OFF		0
 #define WLAN_EN_HIGH		1
@@ -220,29 +224,30 @@ struct cnss_wlan_mac_addr {
  */
 static struct cnss_data {
 	struct platform_device *pldev;
+#ifdef CONFIG_MSM_SUBSYSTEM_RESTART
 	struct subsys_device *subsys;
 	struct subsys_desc    subsysdesc;
+        void *subsys_handle;
+#endif
 	struct cnss_wlan_mac_addr wlan_mac_addr;
 	bool is_wlan_mac_set;
+#ifdef CONFIG_MSM_MEMORY_DUMP
 	bool ramdump_dynamic;
 	struct ramdump_device *ramdump_dev;
 	unsigned long ramdump_size;
 	void *ramdump_addr;
 	phys_addr_t ramdump_phys;
 	struct msm_dump_data dump_data;
+#endif
 	struct cnss_wlan_driver *driver;
 	struct pci_dev *pdev;
 	const struct pci_device_id *id;
-	struct dma_iommu_mapping *smmu_mapping;
-	dma_addr_t smmu_iova_start;
-	size_t smmu_iova_len;
 	struct cnss_wlan_vreg_info vreg_info;
 	bool wlan_en_vreg_support;
 	struct cnss_wlan_gpio_info gpio_info;
 	bool pcie_link_state;
 	bool pcie_link_down_ind;
 	bool pci_register_again;
-	bool notify_modem_status;
 	struct pci_saved_state *saved_state;
 	u16 revision_id;
 	bool recovery_in_progress;
@@ -257,8 +262,9 @@ struct cnss_wlan_mac_addr {
 	struct msm_bus_scale_pdata *bus_scale_table;
 	uint32_t bus_client;
 	int current_bandwidth_vote;
-	void *subsys_handle;
+#ifdef CONFIG_ESOC_CLIENT
 	struct esoc_desc *esoc_desc;
+#endif
 	struct cnss_platform_cap cap;
 	struct msm_pcie_register_event event_reg;
 	struct wakeup_source ws;
@@ -305,7 +311,7 @@ static void cnss_put_wlan_enable_gpio(void)
 
 static int cnss_wlan_vreg_on(struct cnss_wlan_vreg_info *vreg_info)
 {
-	int ret;
+	int ret = 0;
 
 	if (vreg_info->wlan_reg_core) {
 		ret = regulator_enable(vreg_info->wlan_reg_core);
@@ -407,7 +413,7 @@ static int cnss_wlan_vreg_on(struct cnss_wlan_vreg_info *vreg_info)
 
 static int cnss_wlan_vreg_off(struct cnss_wlan_vreg_info *vreg_info)
 {
-	int ret;
+	int ret = 0;
 
 	if (vreg_info->soc_swreg) {
 		ret = regulator_disable(vreg_info->soc_swreg);
@@ -690,27 +696,25 @@ static int cnss_get_wlan_enable_gpio(
 	int ret = 0;
 	struct device *dev = &pdev->dev;
 
-	if (!of_find_property(dev->of_node, gpio_info->name, NULL)) {
-		gpio_info->prop = false;
-		return -ENODEV;
-	}
+	gpio_info->prop = false;
+	if (of_find_property(dev->of_node, gpio_info->name, NULL)) {
+		gpio_info->prop = true;
+		ret = of_get_named_gpio(dev->of_node, gpio_info->name, 0);
+		if (ret >= 0) {
+			gpio_info->num = ret;
+		} else {
+			if (ret == -EPROBE_DEFER)
+				pr_debug("get WLAN_EN GPIO probe defer\n");
+			else
+				pr_err("can't get gpio %s ret %d",
+					gpio_info->name, ret);
+		}
 
-	gpio_info->prop = true;
-	ret = of_get_named_gpio(dev->of_node, gpio_info->name, 0);
-	if (ret >= 0) {
-		gpio_info->num = ret;
-	} else {
-		if (ret == -EPROBE_DEFER)
-			pr_debug("get WLAN_EN GPIO probe defer\n");
-		else
-			pr_err(
-			"can't get gpio %s ret %d", gpio_info->name, ret);
+		ret = cnss_wlan_gpio_init(gpio_info);
+		if (ret)
+			pr_err("gpio init failed\n");
 	}
 
-	ret = cnss_wlan_gpio_init(gpio_info);
-	if (ret)
-		pr_err("gpio init failed\n");
-
 	return ret;
 }
 
@@ -802,20 +806,12 @@ static int cnss_wlan_get_resources(struct platform_device *pdev)
 
 	vreg_info->wlan_reg = regulator_get(&pdev->dev, WLAN_VREG_NAME);
 
-	if (IS_ERR(vreg_info->wlan_reg)) {
-		if (PTR_ERR(vreg_info->wlan_reg) == -EPROBE_DEFER)
-			pr_err("%s: vreg probe defer\n", __func__);
-		else
-			pr_err("%s: vreg regulator get failed\n", __func__);
-		ret = PTR_ERR(vreg_info->wlan_reg);
-		goto err_reg_get;
-	}
-
-	ret = regulator_enable(vreg_info->wlan_reg);
-
-	if (ret) {
-		pr_err("%s: vreg initial vote failed\n", __func__);
-		goto err_reg_enable;
+	if (vreg_info->wlan_reg) {
+		ret = regulator_enable(vreg_info->wlan_reg);
+		if (ret) {
+			pr_err("%s: vreg initial vote failed\n", __func__);
+			goto err_reg_enable;
+		}
 	}
 
 	if (of_get_property(node, WLAN_VREG_SP2T_NAME "-supply", NULL)) {
@@ -852,7 +848,7 @@ static int cnss_wlan_get_resources(struct platform_device *pdev)
 				goto err_ant_switch_set;
 			}
 
-			ret = regulator_set_optimum_mode(vreg_info->ant_switch,
+			ret = regulator_set_load(vreg_info->ant_switch,
 							 WLAN_ANT_SWITCH_CURR);
 			if (ret < 0) {
 				pr_err("%s: Set ant_switch current failed!\n",
@@ -917,16 +913,11 @@ static int cnss_wlan_get_resources(struct platform_device *pdev)
 	}
 	vreg_info->state = VREG_ON;
 
-	ret = cnss_get_wlan_bootstrap_gpio(pdev);
-	if (ret) {
+	if (cnss_get_wlan_bootstrap_gpio(pdev))
 		pr_err("%s: Failed to enable wlan bootstrap gpio\n", __func__);
-		goto err_gpio_wlan_bootstrap;
-	}
 
 	return ret;
 
-err_gpio_wlan_bootstrap:
-	cnss_put_wlan_enable_gpio();
 err_gpio_wlan_en:
 err_wlan_en_reg_get:
 	vreg_info->wlan_en_reg = NULL;
@@ -958,7 +949,6 @@ static int cnss_wlan_get_resources(struct platform_device *pdev)
 
 err_reg_enable:
 	regulator_put(vreg_info->wlan_reg);
-err_reg_get:
 	cnss_disable_xtal_ldo(pdev);
 
 err_reg_xtal_enable:
@@ -998,7 +988,8 @@ static void cnss_wlan_release_resources(void)
 		regulator_put(vreg_info->ant_switch);
 	if (vreg_info->wlan_reg_sp2t)
 		regulator_put(vreg_info->wlan_reg_sp2t);
-	regulator_put(vreg_info->wlan_reg);
+	if (vreg_info->wlan_reg)
+		regulator_put(vreg_info->wlan_reg);
 	if (vreg_info->wlan_reg_xtal)
 		regulator_put(vreg_info->wlan_reg_xtal);
 	if (vreg_info->wlan_reg_xtal_aon)
@@ -1429,65 +1420,6 @@ static int cnss_wlan_is_codeswap_supported(u16 revision)
 	}
 }
 
-static int cnss_smmu_init(struct device *dev)
-{
-	struct dma_iommu_mapping *mapping;
-	int disable_htw = 1;
-	int atomic_ctx = 1;
-	int ret;
-
-	mapping = arm_iommu_create_mapping(&platform_bus_type,
-					   penv->smmu_iova_start,
-					   penv->smmu_iova_len);
-	if (IS_ERR(mapping)) {
-		pr_err("%s: create mapping failed, err = %d\n", __func__, ret);
-		ret = PTR_ERR(mapping);
-		goto map_fail;
-	}
-
-	ret = iommu_domain_set_attr(mapping->domain,
-			      DOMAIN_ATTR_COHERENT_HTW_DISABLE,
-			      &disable_htw);
-	if (ret) {
-		pr_err("%s: set disable_htw attribute failed, err = %d\n",
-			__func__, ret);
-		goto set_attr_fail;
-	}
-
-	ret = iommu_domain_set_attr(mapping->domain,
-				    DOMAIN_ATTR_ATOMIC,
-				    &atomic_ctx);
-	if (ret) {
-		pr_err("%s: set atomic_ctx attribute failed, err = %d\n",
-			__func__, ret);
-		goto set_attr_fail;
-	}
-
-	ret = arm_iommu_attach_device(dev, mapping);
-	if (ret) {
-		pr_err("%s: attach device failed, err = %d\n", __func__, ret);
-		goto attach_fail;
-	}
-
-	penv->smmu_mapping = mapping;
-
-	return ret;
-
-attach_fail:
-set_attr_fail:
-	arm_iommu_release_mapping(mapping);
-map_fail:
-	return ret;
-}
-
-static void cnss_smmu_remove(struct device *dev)
-{
-	arm_iommu_detach_device(dev);
-	arm_iommu_release_mapping(penv->smmu_mapping);
-
-	penv->smmu_mapping = NULL;
-}
-
 #ifdef CONFIG_PCI_MSM
 struct pci_saved_state *cnss_pci_store_saved_state(struct pci_dev *dev)
 {
@@ -1542,7 +1474,7 @@ int cnss_msm_pcie_pm_control(
 		enum msm_pcie_pm_opt pm_opt, u32 bus_num,
 		struct pci_dev *pdev, u32 options)
 {
-	return -ENODEV;
+	return 0;
 }
 
 int cnss_pci_load_and_free_saved_state(
@@ -1553,27 +1485,27 @@ int cnss_pci_load_and_free_saved_state(
 
 int cnss_msm_pcie_shadow_control(struct pci_dev *dev, bool enable)
 {
-	return -ENODEV;
+	return 0;
 }
 
 int cnss_msm_pcie_deregister_event(struct msm_pcie_register_event *reg)
 {
-	return -ENODEV;
+	return 0;
 }
 
 int cnss_msm_pcie_recover_config(struct pci_dev *dev)
 {
-	return -ENODEV;
+	return 0;
 }
 
 int cnss_msm_pcie_register_event(struct msm_pcie_register_event *reg)
 {
-	return -ENODEV;
+	return 0;
 }
 
 int cnss_msm_pcie_enumerate(u32 rc_idx)
 {
-	return -EPROBE_DEFER;
+	return 0;
 }
 #endif
 
@@ -1582,7 +1514,9 @@ static void cnss_pcie_set_platform_ops(struct device *dev)
 	struct cnss_dev_platform_ops *pf_ops = &penv->platform_ops;
 
 	pf_ops->request_bus_bandwidth = cnss_pci_request_bus_bandwidth;
+#ifdef CONFIG_MSM_MEMORY_DUMP
 	pf_ops->get_virt_ramdump_mem = cnss_pci_get_virt_ramdump_mem;
+#endif
 	pf_ops->device_self_recovery = cnss_pci_device_self_recovery;
 	pf_ops->schedule_recovery_work = cnss_pci_schedule_recovery_work;
 	pf_ops->device_crashed = cnss_pci_device_crashed;
@@ -1618,15 +1552,6 @@ static int cnss_wlan_pci_probe(struct pci_dev *pdev,
 	penv->fw_available = false;
 	penv->device_id = pdev->device;
 
-	if (penv->smmu_iova_len) {
-		ret = cnss_smmu_init(&pdev->dev);
-		if (ret) {
-			pr_err("%s: SMMU init failed, err = %d\n",
-				__func__, ret);
-			goto smmu_init_fail;
-		}
-	}
-
 	if (penv->pci_register_again) {
 		pr_debug("%s: PCI re-registration complete\n", __func__);
 		penv->pci_register_again = false;
@@ -1720,7 +1645,6 @@ static int cnss_wlan_pci_probe(struct pci_dev *pdev,
 	dma_free_coherent(dev, EVICT_BIN_MAX_SIZE, cpu_addr, dma_handle);
 err_unknown:
 err_pcie_suspend:
-smmu_init_fail:
 	cnss_pcie_reset_platform_ops(dev);
 	return ret;
 }
@@ -1735,9 +1659,6 @@ static void cnss_wlan_pci_remove(struct pci_dev *pdev)
 	dev = &penv->pldev->dev;
 	cnss_pcie_reset_platform_ops(dev);
 	device_remove_file(dev, &dev_attr_wlan_setup);
-
-	if (penv->smmu_mapping)
-		cnss_smmu_remove(&pdev->dev);
 }
 
 static int cnss_wlan_pci_suspend(struct device *dev)
@@ -1877,11 +1798,11 @@ static int cnss_pm_notify(struct notifier_block *b,
 	.notifier_call = cnss_pm_notify,
 };
 
-static DEFINE_PCI_DEVICE_TABLE(cnss_wlan_pci_id_table) = {
-	{ QCA6174_VENDOR_ID, QCA6174_DEVICE_ID, PCI_ANY_ID, PCI_ANY_ID },
-	{ QCA6174_VENDOR_ID, BEELINER_DEVICE_ID, PCI_ANY_ID, PCI_ANY_ID },
-	{ QCA6180_VENDOR_ID, QCA6180_DEVICE_ID, PCI_ANY_ID, PCI_ANY_ID },
-	{ 0 }
+static const struct pci_device_id cnss_wlan_pci_id_table[] = {
+	{ PCI_DEVICE(QCA6174_VENDOR_ID, QCA6174_DEVICE_ID) },
+	{ PCI_DEVICE(QCA6174_VENDOR_ID, BEELINER_DEVICE_ID) },
+	{ PCI_DEVICE(QCA6180_VENDOR_ID, QCA6180_DEVICE_ID) },
+	{}
 };
 MODULE_DEVICE_TABLE(pci, cnss_wlan_pci_id_table);
 
@@ -2384,9 +2305,6 @@ int cnss_wlan_register_driver(struct cnss_wlan_driver *driver)
 		}
 	}
 
-	if (penv->notify_modem_status && wdrv->modem_status)
-		wdrv->modem_status(pdev, penv->modem_current_status);
-
 	return ret;
 
 err_wlan_probe:
@@ -2439,9 +2357,11 @@ void cnss_wlan_unregister_driver(struct cnss_wlan_driver *driver)
 		return;
 	}
 
+#ifdef CONFIG_MSM_BUS_SCALING
 	if (penv->bus_client)
 		msm_bus_scale_client_update_request(penv->bus_client,
 						    CNSS_BUS_WIDTH_NONE);
+#endif
 
 	if (!pdev) {
 		pr_err("%d: invalid pdev\n", __LINE__);
@@ -2521,6 +2441,7 @@ void cnss_pci_schedule_recovery_work(void)
 	schedule_work(&cnss_pci_recovery_work);
 }
 
+#ifdef CONFIG_MSM_MEMORY_DUMP
 void *cnss_pci_get_virt_ramdump_mem(unsigned long *size)
 {
 	if (!penv || !penv->pldev)
@@ -2530,15 +2451,21 @@ void *cnss_pci_get_virt_ramdump_mem(unsigned long *size)
 
 	return penv->ramdump_addr;
 }
+#endif
 
 void cnss_pci_device_crashed(void)
 {
+#ifdef CONFIG_MSM_SUBSYSTEM_RESTART
 	if (penv && penv->subsys) {
 		subsys_set_crash_status(penv->subsys, true);
 		subsystem_restart_dev(penv->subsys);
 	}
+#else
+	cnss_pci_device_self_recovery();
+#endif
 }
 
+#ifdef CONFIG_MSM_MEMORY_DUMP
 void *cnss_get_virt_ramdump_mem(unsigned long *size)
 {
 	if (!penv || !penv->pldev)
@@ -2549,17 +2476,13 @@ void *cnss_get_virt_ramdump_mem(unsigned long *size)
 	return penv->ramdump_addr;
 }
 EXPORT_SYMBOL(cnss_get_virt_ramdump_mem);
+#endif
 
-void cnss_device_crashed(void)
-{
-	if (penv && penv->subsys) {
-		subsys_set_crash_status(penv->subsys, true);
-		subsystem_restart_dev(penv->subsys);
-	}
-}
-EXPORT_SYMBOL(cnss_device_crashed);
-
+#ifdef CONFIG_MSM_SUBSYSTEM_RESTART
 static int cnss_shutdown(const struct subsys_desc *subsys, bool force_stop)
+#else
+static int cnss_shutdown(void *subsys, bool force_stop)
+#endif
 {
 	struct cnss_wlan_driver *wdrv;
 	struct pci_dev *pdev;
@@ -2603,7 +2526,11 @@ static int cnss_shutdown(const struct subsys_desc *subsys, bool force_stop)
 	return ret;
 }
 
+#ifdef CONFIG_MSM_SUBSYSTEM_RESTART
 static int cnss_powerup(const struct subsys_desc *subsys)
+#else
+static int cnss_powerup(void *subsys)
+#endif
 {
 	struct cnss_wlan_driver *wdrv;
 	struct pci_dev *pdev;
@@ -2679,9 +2606,6 @@ static int cnss_powerup(const struct subsys_desc *subsys)
 			goto err_pcie_link_up;
 	}
 
-	if (penv->notify_modem_status && wdrv->modem_status)
-		wdrv->modem_status(pdev, penv->modem_current_status);
-
 out:
 	penv->recovery_in_progress = false;
 	return ret;
@@ -2734,6 +2658,7 @@ void cnss_pci_device_self_recovery(void)
 	penv->recovery_in_progress = false;
 }
 
+#ifdef CONFIG_MSM_MEMORY_DUMP
 static int cnss_ramdump(int enable, const struct subsys_desc *subsys)
 {
 	struct ramdump_segment segment;
@@ -2753,7 +2678,9 @@ static int cnss_ramdump(int enable, const struct subsys_desc *subsys)
 
 	return do_ramdump(penv->ramdump_dev, &segment, 1);
 }
+#endif
 
+#ifdef CONFIG_MSM_SUBSYSTEM_RESTART
 static void cnss_crash_shutdown(const struct subsys_desc *subsys)
 {
 	struct cnss_wlan_driver *wdrv;
@@ -2768,6 +2695,7 @@ static void cnss_crash_shutdown(const struct subsys_desc *subsys)
 	if (pdev && wdrv && wdrv->crash_shutdown)
 		wdrv->crash_shutdown(pdev);
 }
+#endif
 
 void cnss_device_self_recovery(void)
 {
@@ -2793,40 +2721,7 @@ void cnss_device_self_recovery(void)
 }
 EXPORT_SYMBOL(cnss_device_self_recovery);
 
-static int cnss_modem_notifier_nb(struct notifier_block *this,
-				  unsigned long code,
-				  void *ss_handle)
-{
-	struct cnss_wlan_driver *wdrv;
-	struct pci_dev *pdev;
-
-	pr_debug("%s: Modem-Notify: event %lu\n", __func__, code);
-
-	if (!penv)
-		return NOTIFY_DONE;
-
-	if (SUBSYS_AFTER_POWERUP == code)
-		penv->modem_current_status = 1;
-	else if (SUBSYS_BEFORE_SHUTDOWN == code)
-		penv->modem_current_status = 0;
-	else
-		return NOTIFY_DONE;
-
-	wdrv = penv->driver;
-	pdev = penv->pdev;
-
-	if (!wdrv || !pdev || !wdrv->modem_status)
-		return NOTIFY_DONE;
-
-	wdrv->modem_status(pdev, penv->modem_current_status);
-
-	return NOTIFY_OK;
-}
-
-static struct notifier_block mnb = {
-	.notifier_call = cnss_modem_notifier_nb,
-};
-
+#ifdef CONFIG_MSM_MEMORY_DUMP
 static int cnss_init_dump_entry(void)
 {
 	struct msm_dump_entry dump_entry;
@@ -2848,17 +2743,21 @@ static int cnss_init_dump_entry(void)
 
 	return msm_dump_data_register(MSM_DUMP_TABLE_APPS, &dump_entry);
 }
+#endif
 
 static int cnss_probe(struct platform_device *pdev)
 {
 	int ret = 0;
+#ifdef CONFIG_ESOC_CLIENT
 	struct esoc_desc *desc;
 	const char *client_desc;
+#endif
 	struct device *dev = &pdev->dev;
 	u32 rc_num;
+#ifdef CONFIG_MSM_MEMORY_DUMP
 	struct resource *res;
 	u32 ramdump_size = 0;
-	u32 smmu_iova_address[2];
+#endif
 
 	if (penv)
 		return -ENODEV;
@@ -2868,7 +2767,9 @@ static int cnss_probe(struct platform_device *pdev)
 		return -ENOMEM;
 
 	penv->pldev = pdev;
+#ifdef CONFIG_ESOC_CLIENT
 	penv->esoc_desc = NULL;
+#endif
 
 	penv->gpio_info.name = WLAN_EN_GPIO_NAME;
 	penv->gpio_info.num = 0;
@@ -2889,41 +2790,17 @@ static int cnss_probe(struct platform_device *pdev)
 		goto err_get_rc;
 	}
 
-	ret = of_property_read_u32(dev->of_node, "qcom,wlan-rc-num", &rc_num);
-	if (ret) {
-		pr_err("%s: Failed to find PCIe RC number!\n", __func__);
-		goto err_get_rc;
-	}
-
-	ret = cnss_msm_pcie_enumerate(rc_num);
-	if (ret) {
-		pr_err("%s: Failed to enable PCIe RC%x!\n", __func__, rc_num);
-		goto err_pcie_enumerate;
-	}
-
-	penv->pcie_link_state = PCIE_LINK_UP;
-
-	penv->notify_modem_status =
-		of_property_read_bool(dev->of_node,
-				      "qcom,notify-modem-status");
-
-	if (penv->notify_modem_status) {
-		ret = of_property_read_string_index(dev->of_node, "esoc-names",
-						    0, &client_desc);
+	if (!of_property_read_u32(dev->of_node, "qcom,wlan-rc-num", &rc_num)) {
+		ret = cnss_msm_pcie_enumerate(rc_num);
 		if (ret) {
-			pr_debug("%s: esoc-names is not defined in DT, SKIP\n",
-				__func__);
-		} else {
-			desc = devm_register_esoc_client(dev, client_desc);
-			if (IS_ERR_OR_NULL(desc)) {
-				ret = PTR_RET(desc);
-				pr_err("%s: can't find esoc desc\n", __func__);
-				goto err_esoc_reg;
-			}
-			penv->esoc_desc = desc;
+			pr_err("%s: Failed to enable PCIe RC%x!\n", __func__, rc_num);
+			goto err_pcie_enumerate;
 		}
 	}
 
+	penv->pcie_link_state = PCIE_LINK_UP;
+
+#ifdef CONFIG_MSM_SUBSYSTEM_RESTART
 	penv->subsysdesc.name = "AR6320";
 	penv->subsysdesc.owner = THIS_MODULE;
 	penv->subsysdesc.shutdown = cnss_shutdown;
@@ -2938,10 +2815,12 @@ static int cnss_probe(struct platform_device *pdev)
 	}
 
 	penv->subsys_handle = subsystem_get(penv->subsysdesc.name);
+#endif
 
 	if (of_property_read_bool(dev->of_node, "qcom,is-dual-wifi-enabled"))
 		penv->dual_wifi_info.is_dual_wifi_enabled = true;
 
+#ifdef CONFIG_MSM_MEMORY_DUMP
 	if (of_property_read_u32(dev->of_node, "qcom,wlan-ramdump-dynamic",
 				&ramdump_size) == 0) {
 		penv->ramdump_addr = dma_alloc_coherent(&pdev->dev,
@@ -2988,31 +2867,13 @@ static int cnss_probe(struct platform_device *pdev)
 	}
 
 skip_ramdump:
-	penv->modem_current_status = 0;
-
-	if (penv->notify_modem_status) {
-		penv->modem_notify_handler =
-			subsys_notif_register_notifier(penv->esoc_desc ?
-						       penv->esoc_desc->name :
-						       "modem", &mnb);
-		if (IS_ERR(penv->modem_notify_handler)) {
-			ret = PTR_ERR(penv->modem_notify_handler);
-			pr_err("%s: Register notifier Failed\n", __func__);
-			goto err_notif_modem;
-		}
-	}
-
-	if (of_property_read_u32_array(dev->of_node,
-				       "qcom,wlan-smmu-iova-address",
-				       smmu_iova_address, 2) == 0) {
-		penv->smmu_iova_start = smmu_iova_address[0];
-		penv->smmu_iova_len = smmu_iova_address[1];
-	}
+#endif
 
 	ret = pci_register_driver(&cnss_wlan_pci_driver);
 	if (ret)
 		goto err_pci_reg;
 
+#ifdef CONFIG_MSM_BUS_SCALING
 	penv->bus_scale_table = 0;
 	penv->bus_scale_table = msm_bus_cl_get_pdata(pdev);
 
@@ -3025,6 +2886,7 @@ static int cnss_probe(struct platform_device *pdev)
 			goto err_bus_reg;
 		}
 	}
+#endif
 	cnss_pm_wake_lock_init(&penv->ws, "cnss_wlock");
 
 	register_pm_notifier(&cnss_pm_notifier);
@@ -3047,16 +2909,14 @@ static int cnss_probe(struct platform_device *pdev)
 	return ret;
 
 err_bus_reg:
+#ifdef CONFIG_MSM_BUS_SCALING
 	if (penv->bus_scale_table)
 		msm_bus_cl_clear_pdata(penv->bus_scale_table);
+#endif
 	pci_unregister_driver(&cnss_wlan_pci_driver);
 
 err_pci_reg:
-	if (penv->notify_modem_status)
-		subsys_notif_unregister_notifier
-			(penv->modem_notify_handler, &mnb);
-
-err_notif_modem:
+#ifdef CONFIG_MSM_MEMORY_DUMP
 	if (penv->ramdump_dev)
 		destroy_ramdump_device(penv->ramdump_dev);
 
@@ -3069,17 +2929,22 @@ static int cnss_probe(struct platform_device *pdev)
 			iounmap(penv->ramdump_addr);
 		}
 	}
+#endif
 
+#ifdef CONFIG_MSM_SUBSYSTEM_RESTART
 	if (penv->subsys_handle)
 		subsystem_put(penv->subsys_handle);
 
 	subsys_unregister(penv->subsys);
 
 err_subsys_reg:
+#endif
+#ifdef CONFIG_ESOC_CLIENT
 	if (penv->esoc_desc)
 		devm_unregister_esoc_client(&pdev->dev, penv->esoc_desc);
 
 err_esoc_reg:
+#endif
 err_pcie_enumerate:
 err_get_rc:
 	cnss_configure_wlan_en_gpio(WLAN_EN_LOW);
@@ -3098,12 +2963,15 @@ static int cnss_remove(struct platform_device *pdev)
 
 	cnss_pm_wake_lock_destroy(&penv->ws);
 
+#ifdef CONFIG_MSM_BUS_SCALING
 	if (penv->bus_client)
 		msm_bus_scale_unregister_client(penv->bus_client);
 
 	if (penv->bus_scale_table)
 		msm_bus_cl_clear_pdata(penv->bus_scale_table);
+#endif
 
+#ifdef CONFIG_MSM_MEMORY_DUMP
 	if (penv->ramdump_addr) {
 		if (penv->ramdump_dynamic) {
 			dma_free_coherent(&pdev->dev, penv->ramdump_size,
@@ -3112,6 +2980,7 @@ static int cnss_remove(struct platform_device *pdev)
 			iounmap(penv->ramdump_addr);
 		}
 	}
+#endif
 
 	cnss_configure_wlan_en_gpio(WLAN_EN_LOW);
 	if (penv->wlan_bootstrap_gpio > 0)
@@ -3134,7 +3003,7 @@ static int cnss_remove(struct platform_device *pdev)
 	.driver = {
 		.name = "cnss",
 		.owner = THIS_MODULE,
-		.of_match_table = cnss_dt_match,
+//		.of_match_table = cnss_dt_match,
 #ifdef CONFIG_CNSS_ASYNC
 		.probe_type = PROBE_PREFER_ASYNCHRONOUS,
 #endif
@@ -3148,16 +3017,22 @@ static int __init cnss_initialize(void)
 
 static void __exit cnss_exit(void)
 {
-	struct platform_device *pdev = penv->pldev;
-
+#ifdef CONFIG_MSM_MEMORY_DUMP
 	if (penv->ramdump_dev)
 		destroy_ramdump_device(penv->ramdump_dev);
-	if (penv->notify_modem_status)
-		subsys_notif_unregister_notifier(penv->modem_notify_handler,
-						&mnb);
+#endif
+
+#ifdef CONFIG_MSM_SUBSYSTEM_RESTART
 	subsys_unregister(penv->subsys);
-	if (penv->esoc_desc)
+#endif
+
+#ifdef CONFIG_ESOC_CLIENT
+	if (penv->esoc_desc) {
+		struct platform_device *pdev = penv->pldev;
 		devm_unregister_esoc_client(&pdev->dev, penv->esoc_desc);
+	}
+#endif
+
 	platform_driver_unregister(&cnss_driver);
 }
 
@@ -3242,6 +3117,7 @@ int cnss_pci_request_bus_bandwidth(int bandwidth)
 	case CNSS_BUS_WIDTH_LOW:
 	case CNSS_BUS_WIDTH_MEDIUM:
 	case CNSS_BUS_WIDTH_HIGH:
+#ifdef CONFIG_MSM_BUS_SCALING
 		ret = msm_bus_scale_client_update_request(
 				penv->bus_client, bandwidth);
 		if (!ret) {
@@ -3250,8 +3126,8 @@ int cnss_pci_request_bus_bandwidth(int bandwidth)
 			pr_err("%s: could not set bus bandwidth %d, ret = %d\n",
 			       __func__, bandwidth, ret);
 		}
+#endif
 		break;
-
 	default:
 		pr_err("%s: Invalid request %d", __func__, bandwidth);
 		ret = -EINVAL;
@@ -3274,6 +3150,7 @@ int cnss_request_bus_bandwidth(int bandwidth)
 	case CNSS_BUS_WIDTH_LOW:
 	case CNSS_BUS_WIDTH_MEDIUM:
 	case CNSS_BUS_WIDTH_HIGH:
+#ifdef CONFIG_MSM_BUS_SCALING
 		ret = msm_bus_scale_client_update_request(
 				penv->bus_client, bandwidth);
 		if (!ret) {
@@ -3282,8 +3159,8 @@ int cnss_request_bus_bandwidth(int bandwidth)
 			pr_err("%s: could not set bus bandwidth %d, ret = %d\n",
 			       __func__, bandwidth, ret);
 		}
+#endif
 		break;
-
 	default:
 		pr_err("%s: Invalid request %d", __func__, bandwidth);
 		ret = -EINVAL;
@@ -3388,8 +3265,11 @@ int cnss_auto_suspend(void)
 	penv->monitor_wake_intr = true;
 	penv->pcie_link_state = PCIE_LINK_DOWN;
 
+#ifdef CONFIG_MSM_BUS_SCALING
 	msm_bus_scale_client_update_request(penv->bus_client,
 					    CNSS_BUS_WIDTH_NONE);
+#endif
+
 out:
 	return ret;
 }
@@ -3426,8 +3306,11 @@ int cnss_auto_resume(void)
 
 	atomic_set(&penv->auto_suspended, 0);
 
+#ifdef CONFIG_MSM_BUS_SCALING
 	msm_bus_scale_client_update_request(penv->bus_client,
 					    penv->current_bandwidth_vote);
+#endif
+
 out:
 	return ret;
 }
@@ -3767,8 +3650,10 @@ int cnss_pcie_power_up(struct device *dev)
 
 static void __cnss_vote_bus_width(struct device *dev, uint32_t option)
 {
+#ifdef CONFIG_MSM_BUS_SCALING
 	if (penv->bus_client)
 		msm_bus_scale_client_update_request(penv->bus_client, option);
+#endif
 }
 
 int cnss_pcie_power_down(struct device *dev)
-- 
1.9.1

