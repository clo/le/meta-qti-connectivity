From 3049d9418c7ede795a8bef81c7c460298f287759 Mon Sep 17 00:00:00 2001
From: Rongjing Liao <liaor@codeaurora.org>
Date: Fri, 13 Apr 2018 17:41:25 +0800
Subject: [PATCH 10/11] cfg80211: fix build error about cfg80211_roam_info

Fix build error about struct cfgf80211_roam_info.

Signed-off-by: Johannes Berg <johannes.berg@intel.com>
Git-commit: 29ce6ecbb83c9185d76e3a7c340c9702d2a54961
Git-repo: git://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211-next.git

Change-Id: I162a4d4aa95bb17c2c7b9853190751ce3179d37f
Signed-off-by: Rongjing Liao <liaor@codeaurora.org>
---
 include/net/cfg80211.h | 45 +++++++++++++++++++++++++++++++++++----------
 net/wireless/core.h    |  8 +-------
 net/wireless/sme.c     | 46 +++++++++++++++++++++++++++++++++++++++++++++-
 3 files changed, 81 insertions(+), 18 deletions(-)

diff --git a/include/net/cfg80211.h b/include/net/cfg80211.h
index 9f99bdd..3c97696 100644
--- a/include/net/cfg80211.h
+++ b/include/net/cfg80211.h
@@ -5040,25 +5040,50 @@ void cfg80211_connect_bss(struct net_device *dev, const u8 *bssid,
 }
 
 /**
- * cfg80211_roamed - notify cfg80211 of roaming
+ * struct cfg80211_roam_info - driver initiated roaming information
  *
- * @dev: network device
  * @channel: the channel of the new AP
- * @bssid: the BSSID of the new AP
+ * @bss: entry of bss to which STA got roamed (may be %NULL if %bssid is set)
+ * @bssid: the BSSID of the new AP (may be %NULL if %bss is set)
  * @req_ie: association request IEs (maybe be %NULL)
  * @req_ie_len: association request IEs length
  * @resp_ie: association response IEs (may be %NULL)
  * @resp_ie_len: assoc response IEs length
+ * @authorized: true if the 802.1X authentication was done by the driver or is
+ *      not needed (e.g., when Fast Transition protocol was used), false
+ *      otherwise. Ignored for networks that don't use 802.1X authentication.
+ */
+struct cfg80211_roam_info {
+        struct ieee80211_channel *channel;
+        struct cfg80211_bss *bss;
+        const u8 *bssid;
+        const u8 *req_ie;
+        size_t req_ie_len;
+        const u8 *resp_ie;
+        size_t resp_ie_len;
+        bool authorized;
+};
+
+/**
+ * cfg80211_roamed - notify cfg80211 of roaming
+ *
+ * @dev: network device
+ * @info: information about the new BSS. struct &cfg80211_roam_info.
  * @gfp: allocation flags
  *
- * It should be called by the underlying driver whenever it roamed
- * from one AP to another while connected.
+ * This function may be called with the driver passing either the BSSID of the
+ * new AP or passing the bss entry to avoid a race in timeout of the bss entry.
+ * It should be called by the underlying driver whenever it roamed from one AP
+ * to another while connected. Drivers which have roaming implemented in
+ * firmware should pass the bss entry to avoid a race in bss entry timeout where
+ * the bss entry of the new AP is seen in the driver, but gets timed out by the
+ * time it is accessed in __cfg80211_roamed() due to delay in scheduling
+ * rdev->event_work. In case of any failures, the reference is released
+ * either in cfg80211_roamed() or in __cfg80211_romed(), Otherwise, it will be
+ * released while diconneting from the current bss.
  */
-void cfg80211_roamed(struct net_device *dev,
-		     struct ieee80211_channel *channel,
-		     const u8 *bssid,
-		     const u8 *req_ie, size_t req_ie_len,
-		     const u8 *resp_ie, size_t resp_ie_len, gfp_t gfp);
+void cfg80211_roamed(struct net_device *dev, struct cfg80211_roam_info *info,
+                     gfp_t gfp);
 
 /**
  * cfg80211_roamed_bss - notify cfg80211 of roaming
diff --git a/net/wireless/core.h b/net/wireless/core.h
index 5f5867f..43c34a2 100644
--- a/net/wireless/core.h
+++ b/net/wireless/core.h
@@ -229,13 +229,7 @@ struct cfg80211_event {
 			struct cfg80211_bss *bss;
 			int status; /* -1 = failed; 0..65535 = status code */
 		} cr;
-		struct {
-			const u8 *req_ie;
-			const u8 *resp_ie;
-			size_t req_ie_len;
-			size_t resp_ie_len;
-			struct cfg80211_bss *bss;
-		} rm;
+		struct cfg80211_roam_info rm;
 		struct {
 			const u8 *ie;
 			size_t ie_len;
diff --git a/net/wireless/sme.c b/net/wireless/sme.c
index 35cc1de..4b6f2c2 100644
--- a/net/wireless/sme.c
+++ b/net/wireless/sme.c
@@ -875,7 +875,7 @@ void __cfg80211_roamed(struct wireless_dev *wdev,
 out:
 	cfg80211_put_bss(wdev->wiphy, bss);
 }
-
+/*
 void cfg80211_roamed(struct net_device *dev,
 		     struct ieee80211_channel *channel,
 		     const u8 *bssid,
@@ -894,8 +894,52 @@ void cfg80211_roamed(struct net_device *dev,
 	cfg80211_roamed_bss(dev, bss, req_ie, req_ie_len, resp_ie,
 			    resp_ie_len, gfp);
 }
+EXPORT_SYMBOL(cfg80211_roamed);*/
+
+/* Consumes info->bss object one way or another */
+void cfg80211_roamed(struct net_device *dev, struct cfg80211_roam_info *info,
+                     gfp_t gfp)
+{
+        struct wireless_dev *wdev = dev->ieee80211_ptr;
+        struct cfg80211_registered_device *rdev = wiphy_to_rdev(wdev->wiphy);
+        struct cfg80211_event *ev;
+        unsigned long flags;
+
+        if (!info->bss) {
+                info->bss = cfg80211_get_bss(wdev->wiphy, info->channel,
+                                             info->bssid, wdev->ssid,
+                                             wdev->ssid_len,
+                                             wdev->conn_bss_type,
+                                             IEEE80211_PRIVACY_ANY);
+        }
+
+        if (WARN_ON(!info->bss))
+                return;
+
+        ev = kzalloc(sizeof(*ev) + info->req_ie_len + info->resp_ie_len, gfp);
+        if (!ev) {
+                cfg80211_put_bss(wdev->wiphy, info->bss);
+                return;
+        }
+
+        ev->type = EVENT_ROAMED;
+        ev->rm.req_ie = ((u8 *)ev) + sizeof(*ev);
+        ev->rm.req_ie_len = info->req_ie_len;
+        memcpy((void *)ev->rm.req_ie, info->req_ie, info->req_ie_len);
+        ev->rm.resp_ie = ((u8 *)ev) + sizeof(*ev) + info->req_ie_len;
+        ev->rm.resp_ie_len = info->resp_ie_len;
+        memcpy((void *)ev->rm.resp_ie, info->resp_ie, info->resp_ie_len);
+        ev->rm.bss = info->bss;
+        ev->rm.authorized = info->authorized;
+
+        spin_lock_irqsave(&wdev->event_lock, flags);
+        list_add_tail(&ev->list, &wdev->event_list);
+        spin_unlock_irqrestore(&wdev->event_lock, flags);
+        queue_work(cfg80211_wq, &rdev->event_work);
+}
 EXPORT_SYMBOL(cfg80211_roamed);
 
+
 /* Consumes bss object one way or another */
 void cfg80211_roamed_bss(struct net_device *dev,
 			 struct cfg80211_bss *bss, const u8 *req_ie,
-- 
1.9.1

